/*
  "The cave.js"

  Engine: There is something not right here.  I feel mutability.

  Coder: That place, is strong in dark side of the code.  Domain of
  impurity it is.  And You must execute.

  Engine: What's in there?

  Coder: Only side effects.
*/

import http from 'http'
import url from 'url'
import path from 'path'
import { promises as fs } from 'fs'
import mimeTypes from './mimeTypes.js'

const port = process.env.PORT || 8000
const viewPath = path.join(process.cwd(), 'public')
const server = http.createServer()

process.on('SIGINT', () => { server.close() })
process.on('exit', (code) => { console.log(`\nExit code: ${code}`) })

server.on('request', async (req, res) => {
  // For now only GET request method is allowed
  if(req.method !== 'GET') {
    res.statusCode = 405        // Method Not Allowed
    res.end()
    return
  }

  const pathname = decodeURIComponent(url.parse(req.url).pathname)

  // Make path safe by resolving it to '/' and replacing all '~' characters
  let filePath = path.join(viewPath, path.resolve('/', pathname.replace(/~/g, '')))

  try {
    // Support index.html in directories
    const fileStats = await fs.stat(filePath, { bigint: false })
    if(fileStats.isDirectory()) filePath = path.join(filePath, 'index.html')

    console.log('http-server GET', filePath)

    const file = await fs.readFile(filePath)
    const contentType = mimeTypes.get(path.extname(filePath))

    res.setHeader('Content-Type', contentType || mimeTypes.get('.txt'))
    res.statusCode = 200
    res.end(file)
  } catch (error) {
    console.error('http-server GET ERROR', error)
    res.statusCode = 404
    res.end()
  }
})

server.listen(port, () => {
  console.log(`Fallow white rabbit at http://localhost:${port}`)
})
