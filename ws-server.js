import WebSocket from 'ws'
import { v4 as uuidv4 } from 'uuid'

const port = process.env.WSPORT || 8001
const connectionMap = new Map()

function closeConnection(id, connection) {
  console.log('ws-server | closeConnection', id)
  if(connection.readyState === WebSocket.CONNECTING ||
     connection.readyState === WebSocket.OPEN) connection.close()
  if(connectionMap.has(id)) connectionMap.delete(id)
}

function send(receiverId, senderId, type, data) {
  console.log('ws-server | send', receiverId, senderId, type, data)
  const connection = connectionMap.get(receiverId)
  if(connection) connection.send(JSON.stringify({ senderId, type, data }))
}

new WebSocket.Server({ port }).on('connection', (connection) => {
  const connectionId = uuidv4()
  const otherConnectionIds = [...connectionMap.keys()]
  connectionMap.set(connectionId, connection)
  console.log('ws-server | on connection', connectionId)
  console.log('ws-server | connectionMap size:', connectionMap.size)

  connection.on('close', () => {
    console.log('ws-server | connection on close', connectionId)
    closeConnection(connectionId, connection)
  })

  connection.on('error', (event) => {
    console.error('ws-server | connection on error', connectionId, event)
    closeConnection(connectionId, connection)
  })

  connection.on('message', (message) => {
    console.log('ws-server | on message', connectionId, message)
    try {
      const { receiverId, type, data } = JSON.parse(message)
      send(receiverId, connectionId, type, data)
    } catch(error) {
      console.log('ws-server | on message | catch error', error)
    }
  })

  send(connectionId, connectionId, 'INIT', otherConnectionIds)
})
