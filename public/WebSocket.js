export default class {
  constructor(url = `ws://${location.hostname}:8001`) {
    this._url = url
    this._connection = undefined
    this._queueMessages = []
    this._onmessageCallbacks = {}
  }

  get _isOpen() {
    return this._connection &&
      this._connection.readyState === WebSocket.OPEN
  }

  _sendQueueMessages() {
    while(this._queueMessages.length)
      this._connection.send(this._queueMessages.shift())
    return this
  }

  sendTo(receiverId, type, data) {
    this._queueMessages.push(JSON.stringify({ receiverId, type, data }))
    if(this._isOpen) this._sendQueueMessages()
    return this
  }
  
  on(type, callback) {
    if(!this._onmessageCallbacks[type])
      this._onmessageCallbacks[type] = new Set()
    this._onmessageCallbacks[type].add(callback)
    return this
  }

  close() {
    if(this._isOpen) this._connection.close()
    return this
  }

  connect() {
    this._connection = new WebSocket(this._url)

    this._connection.onopen = () => {
      this._sendQueueMessages()
    }

    this._connection.onmessage = (event) => {
      let message

      try {
        message = JSON.parse(event.data)
      } catch(error) {
        console.warn('WebSocket | onmessage | JSON.parse | catch', error)
      }

      const { senderId, type, data } = message
      const callbackSet = this._onmessageCallbacks[type]

      if(callbackSet) callbackSet.forEach((callback) => {
        console.log(senderId, 'ws.on', type, data)
        callback(senderId, data)
      })
    }

    this._connection.onclose = (event) => {
      console.warn('WebSocket | onclose', event)
      this.close()
    }

    this._connection.onerror = (event) => {
      console.error('WebSocket | onerror', event)
      this.close()
    }

    return this
  }
}
