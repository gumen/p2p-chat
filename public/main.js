import WebSocketConnection from './WebSocket.js'

const elements = {
  form        : document.querySelector('form'),
  textarea    : document.querySelector('textarea'),
  inputSubmit : document.querySelector('input[type="submit"]'),
  messages    : document.getElementById('messages'),
}

const peerConnectionMap = new Map()
const ws = new WebSocketConnection()

function onIcecandidate(connectionId, candidate) {
  console.log(connectionId, 'on icecandidate ws.sendTo', candidate)
  if(candidate) ws.sendTo(connectionId, 'ICECANDIDATE', candidate)
}

function onMessage(connectionId, message, isMyMessage = false) {
  console.log(connectionId, 'onMessage', message, 'isMyMessage:', isMyMessage)

  let parsedMessage
  try {
    parsedMessage = JSON.parse(message)
  } catch(error) {
    console.error('onMessage | JSON.parse error', error)
  }

  if(parsedMessage) {
    const elementListItem = document.createElement('li')

    // TODO handle order using timestamp
    elementListItem.dataset.timestamp = parsedMessage.timestamp

    if(isMyMessage) elementListItem.classList.add('my-message')

    parsedMessage.text.trim().split('\n').forEach((line) => {
      const elementLine = document.createElement('p')
      elementLine.textContent = line
      elementListItem.appendChild(elementLine)
    })

    elements.messages.appendChild(elementListItem)
  }
}

function onReady(connectionId) {
  console.log(connectionId, 'onReady')
  elements.textarea.disabled = false
  elements.inputSubmit.disabled = false
}

function onClose(connectionId, event) {
  console.log(connectionId, 'onClose')
  if(peerConnectionMap.has(connectionId)) {
    peerConnectionMap.get(connectionId).connection.close()
    peerConnectionMap.delete(connectionId)
  }
  if(peerConnectionMap.size === 0) {
    elements.textarea.disabled = true
    elements.inputSubmit.disabled = true
  }
}

ws.on('INIT', async (connectionId, otherConnectionIds) => {
  // send message
  elements.form.onsubmit = (event) => {
    event.preventDefault()

    const message = JSON.stringify({
      timestamp: new Date().getTime(),
      text: elements.textarea.value,
    })

    onMessage(connectionId, message, true)
    peerConnectionMap.forEach(({ channel }) => channel.send(message))
    elements.textarea.value = ''
  }

  ws.on('ICECANDIDATE', async (senderId, candidate) => {
    if (peerConnectionMap.has(senderId)) {
      const { connection } = peerConnectionMap.get(senderId)
      await connection.addIceCandidate(candidate)
    }
  })

  ws.on('CREATE_REMOTE_CONNECTION', (senderId) => {
    const connection = new RTCPeerConnection()

    connection.addEventListener('icecandidate', ({ candidate }) =>
      onIcecandidate(senderId, candidate))

    connection.addEventListener('datachannel', (event) => {
      console.log(senderId, 'on datachannel')

      const channel = event.channel
      channel.binaryType = 'arraybuffer'
      channel.addEventListener('close', onClose.bind(null, senderId))
      channel.addEventListener('message', ({ data }) =>
        onMessage(senderId, data))
      onReady(senderId)

      peerConnectionMap.set(senderId, {
        ...peerConnectionMap.get(senderId),
        channel,
      })
    })

    peerConnectionMap.set(senderId, { connection })
    ws.sendTo(senderId, 'CREATE_LOCAL_CONNECTION')
  })

  ws.on('CREATE_LOCAL_CONNECTION', async (senderId) => {
    const connection = new RTCPeerConnection()

    connection.addEventListener('icecandidate', ({ candidate }) =>
      onIcecandidate(senderId, candidate))

    const channel = connection.createDataChannel('messaging-channel', {
      ordered: true
    })
    channel.binaryType = 'arraybuffer'
    channel.addEventListener('close', onClose.bind(null, senderId))
    channel.addEventListener('message', ({ data }) =>
      onMessage(senderId, data))
    channel.addEventListener('open', onReady.bind(null, senderId))

    peerConnectionMap.set(senderId, { connection, channel })

    const offer = await connection.createOffer()
    await connection.setLocalDescription(offer)
    ws.sendTo(senderId, 'OFFER', offer)
  })

  ws.on('OFFER', async (senderId, offer) => {
    if (peerConnectionMap.has(senderId)) {
      const { connection } = peerConnectionMap.get(senderId)
      await connection.setRemoteDescription(offer)
      const answer = await connection.createAnswer()
      await connection.setLocalDescription(answer)
      ws.sendTo(senderId, 'ANSWER', answer)
    }
  })

  ws.on('ANSWER', async (senderId, answer) => {
    if (peerConnectionMap.has(senderId)) {
      const { connection } = peerConnectionMap.get(senderId)
      await connection.setRemoteDescription(answer)
    }
  })

  otherConnectionIds.forEach((otherConnectionId) => {
    ws.sendTo(otherConnectionId, 'CREATE_REMOTE_CONNECTION')
  })
})

ws.connect()
