# How to run NodeJS HTTPS server on localhost on Linux.

## Step 1: Root SSL certificate

In case of issues *(Unable to load Private Key...)* with second
command make sure that generated key file (`rootCA.key`) is encoded
in `UTF-8` and NOT `UTF-8-<something_elese>`.

```sh
$ openssl genrsa -des3 -out root.cert.key
$ openssl req -x509 -new -nodes -key rootCA.key -sha 256 -days 1024 -out rootCA.pem
```

## Step 2: Import root CA certificate

### Add root CA to system

This step is actually not important.  Browsers will not use system
knowledge of root CA.  See next step on how add root CA to browsers.

```sh
$ sudo mkdir /usr/local/share/cs-certificates/extra
$ sudo cp rootCA.pem /usr/local/share/cs-certificates/extra/root.cert.crt
$ sudo update-ca-certificates
```

### Add root CA to web browsers

```sh
$ sudo apt install libnss3-tools
```

Create and run shell script to avoid manual labor.

```bash
#!/bin/bash
# Install rootCA.pem to certificate trust store of applications using NSS

# CA file to install
certfile="rootCA.pem"
certname="My Root CA"

# For cert8 (legacy - DBM) Mozilla
for certDB in $(find ~/ -name "cert8.db")
do
	certdir=$(dirname ${certDB})
	certutil -A -n "${certname}" -t "TCu,Cu,Tu" -i ${certfile} -d dbm:${certdir}
done

# For cert9 (SQL) Chrome
for certDB in $(find ~/ -name "cert9.db")
do
	certdir=$(dirname ${certDB})
	certutil -A -n "${certname}" -t "TCu,Cu,Tu" -i ${certfile} -d sql:${certdir}
done
```

## Step 3: Domain SSL certificate for localhost

Create configuration file `srver.csr.cnf`:

```conf
[req]
default_bits = 2048
prompt = no
default_md = sha256
distinguished_name = dn

[dn]
C=US
ST=RandomState
L=RandomCity
O=RandomOrganization
OU=RandomOrganizationUnit
emailAddress=hello@example.com
CN = localhost
```

Create `v3.ext` file:

```conf
authorityKeyIdentifier=keyid,issuer
basicConstraints=CA:FALSE
keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment
subjectAltName = @alt_names

[alt_names]
DNS.1 = localhost
```

```sh
$ openssl req -new -sha256 -nodes -out server.csr -newkey rsa:2048 -keyout server.key -config <( cat server.csr.cnf )
$ openssl x509 -req -in server.csr -CA rootCA.pem -CAkey rootCA.key -CAcreateserial -out server.crt -days 500 -sha256 -extfile v3.ext
```

## Step 4: Create HTTPS NodeJS server

It's basically the same as HTTP server but requires to load files:
`server.key` and `server.crt` that we create in previous step.

`https-server.mjs`:

```js
import https from 'https'
import { promises as fs } from 'fs'

const server = https.createServer({
  key  : await fs.readFile('path_to_file/server.key'),
  cert : await fs.readFile('path_to_file/server.crt'),
})

server.on('request', async (req, res) => {
  res.statusCode = 200
  res.end('Hello World')
})

server.listen(8000, () => console.log('open https://localhost:8000'))
```

```sh
$ node --version // v14.4.0 (or higher)
$ node --experimental-top-level-await https.server.mjs
```

## Useful Links / Sources

- [What is Root SSL Certificate?](https://support.dnsimple.com/articles/what-is-ssl-root-certificate/)
- [How to get HTTPS working on your local development environment in 5 minutes](https://www.freecodecamp.org/news/how-to-get-https-working-on-your-local-development-environment-in-5-minutes-7af615770eec/)
- [Unable to load Private Key](https://stackoverflow.com/questions/18460035/unable-to-load-private-key-pem-routinespem-read-biono-start-linepem-lib-c6)
- [Emacs - changing encodings](https://www.emacswiki.org/emacs/ChangingEncodings)
- [How to import CA root certificates on Linux and Windows](https://thomas-leister.de/en/how-to-import-ca-root-certificate/)
